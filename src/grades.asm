;***************************************
;	grades.asm
;	Date: 20 Mar 2017
;	Homework: 
;		user enters score and letter grade is show
;	Author: Jose Madureira
;***************************************

Include Irvine32.inc
.data
	score DWORD ?
	grade BYTE ?
	msgGetScore BYTE "Please Input your score: ",0
	msgScored BYTE "    Your Grade: ",0
	msgErr BYTE "    /!\ ERROR: Invalid score /!\",0

.code
Main PROC
;	Get score from the user
	mov EDX, OFFSET msgGetScore
	call WriteString
	call ReadDec
	mov score, EAX
	call Crlf

;	score above 100, possible error
	cmp score, 100
	ja LERR			;throw error

	cmp score, 90
	ja LA

	cmp score, 80
	ja LB

	cmp score, 70
	ja LC

	cmp score, 60
	ja lD

	cmp score, 0
	ja lF

;	else the score is below zero, possible error
	jmp LErr

LA:	; 90 > score >= 100
	mov grade, 'A'		;A
	jmp LShow

LB:	; 80 > score >= 90
	mov grade, 'B'		;B
	jmp LShow	

LC:	; 70 > score >= 80
	mov grade, 'C'		;C
	jmp LShow
	
LD:	; 60 > score >= 70
	mov grade, 'D'		;D
	jmp LShow

LF:	; 0 > score >= 60
	mov grade, 'F'		;F
	jmp LShow

LErr:
	mov EDX, OFFSET msgErr
	call WriteString
	jmp LEnd

LShow:
	mov EDX, OFFSET msgScored
	call WriteString

	mov AL, grade		;store letter in AL
	call WriteChar
	call Crlf
	jmp LEnd

LEnd:
	call Crlf

exit
Main ENDP
END Main
